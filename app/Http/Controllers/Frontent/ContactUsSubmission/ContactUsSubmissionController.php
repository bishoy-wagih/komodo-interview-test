<?php

namespace App\Http\Controllers\Frontent\ContactUsSubmission;

use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\ContactSubmissioStoreRequest;
use App\Repositories\Frontend\ContactUsSubmissionRepository;

class ContactUsSubmissionController extends Controller
{
    private $contactRepository;

    public function __construct(ContactUsSubmissionRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function create(): View
    {
        return view('frontend.contact_us_submissions.create');
    }

    public function store(ContactSubmissioStoreRequest $request)
    {
        $this->contactRepository->store($request->all());

        return redirect()->route('frontend.contact-us.create')->withFlashSuccess('Thank you for contacting us, we will get back to you shortly!');
    }
}
