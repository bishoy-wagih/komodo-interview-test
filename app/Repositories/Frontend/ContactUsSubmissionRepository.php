<?php

namespace App\Repositories\Frontend;

use App\Models\ContactUsSubmission;
use Illuminate\Database\Eloquent\Collection;

class ContactUsSubmissionRepository
{
    private $model;
    /**
     * Inject Model to repository
     */
    public function __construct(ContactUsSubmission $model)
    {
        $this->model = $model;
    }

    /**
     * Store Form data to database
     * @param  array $data 
     * @return ContactUsSubmission $contact form.
     */
    public function store(array $data) :ContactUsSubmission
    {
        return $this->model::create($data);
    }
}
