<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @if(session()->has('flash_success'))
            @php
                $message = session('flash_success');
            @endphp
            <x-alert type="success" :message="$message"></x-alert>
        @endif

        @if(session()->has('flash_error'))
            @php
                $message = session('flash_error');
            @endphp
            <x-alert type="danger" :message="$message"></x-alert>
        @endif

        @if ($errors->any())

        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        </div>
    </div>
</div>
