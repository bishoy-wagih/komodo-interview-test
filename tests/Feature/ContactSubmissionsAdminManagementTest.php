<?php

namespace Tests\Feature;

use App\Models\ContactUsSubmission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ContactSubmissionsAdminManagementTest extends TestCase
{
    use RefreshDatabase;

    protected $submission;

    public function setUp(): void
    {
        parent::setUp();

        $this->submission = ContactUsSubmission::factory()->create();
    }

    /**
     * @test
     */
    public function guest_use_cant_access_submissions_list_page()
    {
        $this->get(route('backend.contact-us.index'))
              ->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function auth_user_only_can_access_submissions_list_page()
    {
        $this->signIn();

        $this->get(route('backend.contact-us.index'))
              ->assertStatus(200);
    }

    /**
     * @test
     */
    public function guest_use_cant_access_submissions_details_page()
    {
        $this->get(route('backend.contact-us.show', $this->submission->id))
              ->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function auth_user_can_see_submission_details()
    {
        $this->signIn();

        $this->get(route('backend.contact-us.show', $this->submission->id))
              ->assertStatus(200)
              ->assertSee($this->submission->name)
              ->assertSee($this->submission->email)
              ->assertSee($this->submission->phone)
              ->assertSee($this->submission->address)
              ->assertSee($this->submission->message);
    }
}
