<?php

namespace Database\Seeders;

use App\Models\ContactUsSubmission;
use Illuminate\Database\Seeder;

class ContactUsSubmissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();

        ContactUsSubmission::factory(30)->create();
    }

    protected function truncateTable()
    {
        ContactUsSubmission::truncate();
    }
}
