@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row border-bottom mb-4 p-2">
            <div class="col-md-4">
                Name
            </div>

            <div class="col-md-8">
                {{ $submission->name }}
            </div>
        </div>

        <div class="row border-bottom mb-4 p-2">
            <div class="col-md-4">
                Email
            </div>

            <div class="col-md-8">
                {{ $submission->email }}
            </div>
        </div>

        <div class="row border-bottom mb-4 p-2">
            <div class="col-md-4">
                Phone
            </div>

            <div class="col-md-8">
                {{ $submission->phone }}
            </div>
        </div>

        <div class="row border-bottom mb-4 p-2">
            <div class="col-md-4">
                Address
            </div>

            <div class="col-md-8">
                {{ $submission->address }}
            </div>
        </div>

        <div class="row border-bottom mb-4 p-2">
            <div class="col-md-4">
                Message
            </div>

            <div class="col-md-8">
                {{ $submission->message }}
            </div>
        </div>

        <div>
            <a href="{{ route('backend.contact-us.index') }}" class="btn btn-primary"> Back </a>
        </div>

    </div>

@endsection

