<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(! $this->checkAdminIsCreated())
        {
            User::create([
                'name'      => 'admin',
                'email'     => User::ADMIN_EMAIL,
                'email_verified_at' => now(),
                'password' => '123456',
                'remember_token' => Str::random(10),
            ]);
        }
    }

    /**
     * check if admin user has added before
     * 
     * @return mixed $user | null
     */
    protected function checkAdminIsCreated(): ?User
    {
        return User::where('email', User::ADMIN_EMAIL)->first();
    }
}
