## Komodo Interview Task Setup Instructions.

 - Application built by latest Laravel version "8"
 - Application uses BitBucket as a version control, Its a public repository, so you can clone it without any permissions.
 https://bitbucket.org/bishoy-wagih/komodo-interview-test/src/master

### Application has two branches ["master", "livewire"].

#### Master Branch:

- submit the contact form in a normal way, its a server side submission with handling form validation.

- render list of submissions in a table paginated for authenticated users only.

#### Livewire Branch:    

- handle contact form submission from livewire component with validation as well.

- render submissions list in livewire component with pagination.

## Seeders:

- Application has two seeders:
- admins seeder, so can easly login to the application.
    - email: admin@test.com
    - password: 123456    
    - Contact Submissions seeder:
        - it runs factory to seed 30 record, so pagination be visible in submissions list table.

## Application Testing:

- I used PHPUnit for testing using sqlite database, changed from phpunit.xml.

- as well livewire compoents are tested.

##  Envirnment Setup:
 - I have an issue using Mysql5,7 docker image on my mac machine since I'm Using MacPro with M1 processor, screenshot attached with the issue.
    
 - I used Mariadb instead and it works fine.     

