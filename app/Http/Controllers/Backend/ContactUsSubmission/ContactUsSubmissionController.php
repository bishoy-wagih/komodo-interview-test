<?php

namespace App\Http\Controllers\Backend\ContactUsSubmission;

use App\Http\Controllers\Controller;
use App\Models\ContactUsSubmission;
use App\Repositories\Backend\ContactUsSubmissionRepository;
use Illuminate\Http\Request;

class ContactUsSubmissionController extends Controller
{
    protected $repository;

    public function __construct(ContactUsSubmissionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $submissions = $this->repository->getPaginated(25, 'id', 'desc');

        return view('backend.contact_us.index', [
            'submissions' => $submissions
        ]);
    }

    public function show(ContactUsSubmission $contactUsSubmission)
    {
        return view('backend.contact_us.show', [
            'submission' => $contactUsSubmission
        ]);
    }
}
