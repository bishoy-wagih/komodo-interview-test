<?php

namespace App\Models\Traits;

trait UserAttributesTrait
{
    public function setPasswordAttribute($plainPassword)
    {
        $this->attributes['password'] = bcrypt($plainPassword);
    }
}
