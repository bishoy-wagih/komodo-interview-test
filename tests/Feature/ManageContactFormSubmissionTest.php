<?php

namespace Tests\Feature;

use App\Models\ContactUsSubmission;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageContactFormSubmissionTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function guest_user_can_access_contact_form_view()
    {
        $this->get(route('frontend.contact-us.create'))
        ->assertSee('name')
        ->assertSee('email')
        ->assertStatus(200);
    }

    /**
     * @test
     */
    public function it_can_submit_the_contact_form()
    {
        $formData = ContactUsSubmission::factory()->raw();

        $response = $this->post(route('frontend.contact-us.store', $formData));
        
        $this->assertDatabaseHas('contact_us_submissions', ['name' => $formData['name'], 'email' => $formData['email']]);

        $response->assertRedirect( route('frontend.contact-us.create') );
    }

    /**
     * @test
     */
    public function it_requires_a_name()
    {
        $formData = ContactUsSubmission::factory()->raw(['name' => '']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors('name');
    }

    /**
     * @test
     */
    public function it_requires_an_email()
    {
        $formData = ContactUsSubmission::factory()->raw(['email' => '']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['email' => 'The email field is required.']);
    }

    /**
     * @test
     */
    public function it_requires_a_valid_email()
    {
        $formData = ContactUsSubmission::factory()->raw(['email' => 'invalidEmailAddress']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['email' => 'The email must be a valid email address.']);
    }

    /**
     * @test
     */
    public function it_requires_a_phone_number()
    {
        $formData = ContactUsSubmission::factory()->raw(['phone' => '']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['phone' => 'The phone field is required.']);
    }

    /**
     * @test
     */
    public function it_requires_phone_number_wih_only_digits()
    {
        $formData = ContactUsSubmission::factory()->raw(['phone' => 'test12345678']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['phone' => 'The phone must be 11 digits.']);
    }

    /**
     * @test
     */
    public function it_requires_phone_number_with_11_digit()
    {
        $formData = ContactUsSubmission::factory()->raw(['phone' => '123456']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['phone' => 'The phone must be 11 digits.']);
    }

    /**
     * @test
     */
    public function it_requires_a_message()
    {
        $formData = ContactUsSubmission::factory()->raw(['message' => '']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['message' => 'The message field is required.']);
    }

    /**
     * @test
     */
    public function it_requires_a_message_with_min_5_chars()
    {
        $formData = ContactUsSubmission::factory()->raw(['message' => 'asd']);
        $response = $this->post(route('frontend.contact-us.store', $formData));
        $response->assertSessionHasErrors(['message' => 'The message must be at least 5 characters.']);
    }
}
