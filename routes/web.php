<?php

use App\Http\Controllers\Backend\ContactUsSubmission\ContactUsSubmissionController as BackendSubmissionsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Frontent\ContactUsSubmission\ContactUsSubmissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('contact-us', [ ContactUsSubmissionController::class, 'create' ])->name('frontend.contact-us.create');

Route::post('contact-us', [ ContactUsSubmissionController::class, 'store' ])->name('frontend.contact-us.store');

Route::prefix('backend')->middleware('auth')->as('backend.')->group(function () {
    Route::get('/contact-us', [BackendSubmissionsController::class, 'index'])->name('contact-us.index');
    Route::get('/contact-us/{contact_us_submission}', [BackendSubmissionsController::class, 'show'])->name('contact-us.show');
});