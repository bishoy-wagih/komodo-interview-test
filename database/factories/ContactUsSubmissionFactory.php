<?php

namespace Database\Factories;

use App\Models\ContactUsSubmission;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactUsSubmissionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ContactUsSubmission::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'       => $this->faker->name,
            'email'      => $this->faker->email,
            'phone'       => $this->faker->numerify('###########'),
            'address'     => $this->faker->address,
            'message'     => $this->faker->paragraph,
        ];
    }
}
