@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">Created At</th>
        <th scope="col">Ago</th>
        <th scope="col">Show</th>
        </tr>
    </thead>
    <tbody>
        @foreach($submissions as $submission)
        <tr>
            <th scope="row">{{ $submission->id }}</th>
            <td>{{ $submission->name }}</td>
            <td>{{ $submission->email }}</td>
            <td>{{ $submission->phone }}</td>
            <td>{{ $submission->created_at->format('Y-m-d H:i') }}</td>
            <td>{{ $submission->created_at->diffForHumans() }}</td>
            <td>
                <a href="{{ route('backend.contact-us.show', $submission->id) }}">
                    Show
                </a>
                
            </td>
        </tr>
        @endforeach
    </tbody>
    </table>

    <div class="d-flex justify-content-center">
        {{ $submissions->render() }}
    </div>

</div>
@endsection

