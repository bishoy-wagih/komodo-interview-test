<?php

namespace App\Repositories\Backend;

use App\Models\ContactUsSubmission;

class ContactUsSubmissionRepository
{
    private $model;
    /**
     * Inject Model to repository
     */
    public function __construct(ContactUsSubmission $model)
    {
        $this->model = $model;
    }

    public function getPaginated($perPage = 25, $orderBy = 'id', $sort = 'asc')
    {
        return $this->model
                ->orderBy($orderBy, $sort)
                ->paginate($perPage);
    }
}
